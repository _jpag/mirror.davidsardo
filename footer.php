<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BrainBlank
 */

?>

	</div><!-- #content -->
<!-- </main> -->
<!-- #barba-js -->

<footer id="colophon" class="c-footer">
	<div class="l-container">
		<div class="c-footer__inner">
			<div class="c-footer__name">
				<?php if (!dynamic_sidebar('footerleft') ) : endif; ?>
			</div>
			<img src="<?php echo get_template_directory_uri() ?>/images/logo-agriturismo-david-sardo-footer.svg" alt="David Sardo - Azienda Agricola Kmetija">
			<div id="form" class="c-footer__info">
				<?php if (!dynamic_sidebar('footerright') ) : endif; ?>
			</div>
		</div>

		<div class="c-footer__bottom">
			<div>
				<div class="inner">
					<div>
						<a href="https://www.facebook.com" target="_blank">
							<img class="social-icon"
								src="<?php echo get_template_directory_uri() ?>/images/social-facebook.svg"
								alt="Facebook">
						</a>
						<a href="https://www.instagram.com/" target="_blank">
							<img class="social-icon"
								src="<?php echo get_template_directory_uri() ?>/images/social-instagram.svg"
								alt="Instagram">
						</a>
					</div>
				</div>
				<div class="inner">
					<p><?php echo date("Y");?>&nbsp;©&nbsp;<?php if (!dynamic_sidebar('footerbottom') ) : endif; ?></p>
				</div>
			</div>
			<a href="http://www.brainupstudio.it" target="_blank">
				<img src="<?php echo get_template_directory_uri() ?>/images/brainup-studio-logo.svg"
					alt="BrainUp Studio - Web design">
			</a>
		</div>
	</div>

</footer>

<?php wp_footer(); ?>

</body>

</html>