<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package BrainBlank
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function brainblank_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'brainblank_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function brainblank_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'brainblank_pingback_header' );

/**
 * ACF Gutenberg Blocks
 */
add_action('acf/init', 'my_acf_init');

function my_acf_init() {
	
	if( function_exists('acf_register_block') ) {	
		// register a slideshow block
		acf_register_block(array(
			'name'				=> 'slideshow',
			'title'				=> __('Slideshow home page'),
			'description'		=> __('A custom slideshow block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'images-alt2',
			'keywords'			=> array( 'slideshow', 'carosello' ),
		));
		// register a maps block
		acf_register_block(array(
			'name'				=> 'maps',
			'title'				=> __('Mappa di Google'),
			'description'		=> __('A custom maps block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'admin-site-alt',
			'keywords'			=> array( 'Maps', 'Mappa' ),
		));
		// register a contact block
		acf_register_block(array(
			'name'				=> 'contact',
			'title'				=> __('Contatti'),
			'description'		=> __('A custom contact block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'email',
			'keywords'			=> array( 'Contact', 'Contatti e form' ),
		));
		// register a paragraph block
		acf_register_block(array(
			'name'				=> 'paragraph',
			'title'				=> __('Paragrafo semplice'),
			'description'		=> __('A custom paragraph block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'editor-alignleft',
			'keywords'			=> array( 'Paragraph', 'Paragrafo semplice' ),
		));
		// register a hero home block
		acf_register_block(array(
			'name'				=> 'hero-home',
			'title'				=> __('Hero home page'),
			'description'		=> __('A custom hero home page block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'laptop',
			'keywords'			=> array( 'Hero', 'Hero home page' ),
		));
		// register a body home block
		acf_register_block(array(
			'name'				=> 'body-home',
			'title'				=> __('Body home page'),
			'description'		=> __('A custom body home page block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'Body home', 'Body home page' ),
		));
		// register a body home block
		acf_register_block(array(
			'name'				=> 'opens',
			'title'				=> __('Calendario aperture'),
			'description'		=> __('A custom body home page block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'calendar-alt',
			'keywords'			=> array( 'Openings calendar', 'Openings calendar' ),
		));
		// register a body home block
		acf_register_block(array(
			'name'				=> 'wine-list',
			'title'				=> __('Lista vini'),
			'description'		=> __('A custom wine list block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'forms',
			'keywords'			=> array( 'Wine list', 'Wine list' ),
		));
		// register a body home block
		acf_register_block(array(
			'name'				=> 'wine-list-simple',
			'title'				=> __('Lista vini semplice'),
			'description'		=> __('A simple wine list block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'forms',
			'keywords'			=> array( 'Simple Wine list', 'Simple Wine list' ),
		));
		acf_update_setting('google_api_key', 'AIzaSyBrCDdwb9CpTMKo50WxZBTpr49i9LEa-gY');
	}
}
function my_acf_block_render_callback( $block ) {	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") );
	}
}

/**
 * Hide editor in template
 */
// add_action('init', 'my_rem_editor_from_post_type');
// function my_rem_editor_from_post_type() {
//     remove_post_type_support( 'post', 'editor' );
// }

/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
	return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);

/**
 * Edit in blank target
 */
add_filter( 'edit_post_link', function( $link, $post_id, $text )
{
    // Add the target attribute 
    if( false === strpos( $link, 'target=' ) )
        $link = str_replace( '<a ', '<a target="_blank" ', $link );

    return $link;
}, 10, 3 );

/**
 * Disable REST API
 */
// add_filter( 'rest_authentication_errors', function( $result ) {
//     if ( ! empty( $result ) ) {
//         return $result;
//     }
//     if ( ! is_user_logged_in() ) {
//         return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
//     }
//     return $result;
// });
