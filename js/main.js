
(function ($) {
    var isMobile = $(window).innerWidth() < 768;
    var showreelInitializer = function () {
        var sl = $(".c-slideshow");
        if (sl.length > 0) {
            sl.cycle({
                timeout: 5000,
                speed: 500,
                slides: '.item',
                swipe: true,
                fx: 'fadeout',
                log: false,
                //swipeFx: 'scrollHorz',
                pager: '.c-slideshow__pager',
                pagerTemplate: '<a href="javascript:;" class="pager"></a>',
                pagerActiveClass: 'isActive'
            });
        }
    };

    var carouselInitializer = function () {
        if ($(window).innerWidth() <= 767) {      
            $('.js-carouselBlog').owlCarousel({
                responsive: {
                    0: {
                        items: 1,
                        margin: 20,
                        stagePadding: 30,
                        loop: true,
                        nav: false,
                        dots: true,
                        center: true,
                    }
                }
            })
        }
        
        $('.js-carouselHome').owlCarousel({
            loop: false,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 20,
                    stagePadding: 30,
                },
                768: {
                    items: 2,
                    margin: 20,
                    stagePadding: 30,
                },
                1024: {
                    items: 3,
                    margin: 40,
                    stagePadding: 50,
                },
                1280: {
                    items: 4,
                    margin: 50,
                    stagePadding: 50,
                },
                1920: {
                    items: 5,
                    margin: 70,
                    stagePadding: 50,
                }
            }
        })
    };

    var togglerMenuMobile = function () {
        $(document).on("click", ".js-menuToggler", function () {
            $('body').toggleClass("openMenu");
        });
    };

    var menuMobile = function (e) {
        var el = $('.js-noPage').children('a');
        var w = $(window).innerWidth();
        if (el.length > 0 && w < 1024) {
            el.on('click', function (e) {
                e.preventDefault();
                el.siblings('.sub-menu').removeClass('isOpen');
                $(this).siblings('.sub-menu').addClass('isOpen');
            });
        }
    };

    var anchorScroll = function () {
        if (isMobile) {
            var topPos = 40;
        } else {
            var topPos = 120;
        }

        $('.js-scrollCalendar').click(function () {
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - topPos
            }, 600);
            return false;
        });

        if ( window.location.hash ) scroll(0,0);
        setTimeout( function() { scroll(0,0); }, 1);

        $(function() {
            // *only* if we have anchor on the url
            if(window.location.hash) {

                // smooth scroll to the anchor id
                $('html, body').animate({
                    scrollTop: ($(window.location.hash).offset().top - topPos) + 'px'
                }, 1000, 'swing');
            }

        });
    };

    var bodyScroll = function () {
        var body = $('body');
        var title = $('.c-page__title');
        
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            body.toggleClass("isScrolled", scroll > 80);
            if (!isMobile) {
                title.toggleClass("isAnimated", scroll > 240);
            }
        });
    };

    var jsAppearInitializer = function () {
        /*
            Per attivare questa animazione basta aggiungere la classe js-appear a qualsiasi elemento.
            Se si vuole aggiungere un delay aggiungere un attributo "js-delay" es js-delay="150"
            (da utilizzare ad es. su elementi sulla stessa fila per non farli comparire tutti in blocco)
        */
        if ($(".js-appear").length > 0 && $(window).innerWidth() > 767) {
            var viewPortHeight = $(window).height();
            var currPosition = $(window).scrollTop();
    
            var isOnScreen = function (el) {
                // console.log('on screen');
                return ($(el).offset().top + 200) < (currPosition + viewPortHeight - ($(el).attr("js-delay") || 0));
            }
            $(".js-appear").each(function () {
                $(this).toggleClass("hidden", !isOnScreen(this));
            });
            var scrollHappened = function () {
                currPosition = $(window).scrollTop();
                $(".js-appear.hidden").each(function () {
                    $(this).toggleClass("hidden", !isOnScreen(this));
                });
            };
    
            $(window).scroll(scrollHappened);
    
            $(window).resize(function () {
                viewPortHeight = $(window).height()
                scrollHappened();
            });
        }
    
    };

    var pageAnimation = function() {
        barba.init({
            transitions: [{
              sync: false, // sync the transitions up so they occur concurrently
              appear({ // when you first load the website.
                current,
              }) {
                const targets = current.container; // target the current page container.
                const a = anime({ // new animations via anime js
                  targets, // our current container
                  opacity: [0, 1], // change opacity from 0 to 1.
                  duration: 1500, // time in ms
                  easing: 'linear', // easing function
                });
          
                return a.finished;
              },
              leave({ // leaving the current page
                current,
              }) {
                const targets = current.container; // target current page
                const a = anime({ // new animation
                  targets, // current page
                  opacity: [1, 0], // fade out from 1 to 0 opacity
                  duration: 1500, //time in ms
                  easing: 'linear', // easing function
                });
          
                return a.finished;
              },
              enter({ // entering the next page
                next,
              }) {
                const targets = next.container; // target the container of the next page
                const a = anime({ // new animation
                  targets, // next page
                  opacity: [0, 1], // fade in from 0 to 1 opacity
                  duration: 1500, // time in ms
                  easing: 'linear', // easing function
                });
          
                return a.finished;
              },
            }],
          });
    };

    $(document).ready(function () {
        //pageAnimation();
        //menuMobile();
        //showreelInitializer();
        //carouselInitializer();
        //jsAppearInitializer();
        togglerMenuMobile();
        anchorScroll();
        bodyScroll();
    })

})(jQuery);
