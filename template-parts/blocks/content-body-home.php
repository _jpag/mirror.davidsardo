<?php 
    $first = get_field('first_group');
    $second = get_field('second_group');
    $third = get_field('third_group');
?>

<section class="c-body__home l-container">
    <div class="c-blockHome c-blockHome__first">
        <div class="c-blockHome__image">
            <div class="u-cover-image">
                <img src="<?php echo $first['image']['url'] ?>" alt="<?php echo $first['image']['title'] ?>">
            </div>
        </div>
        <div class="c-blockHome__content">
            <h2 class="c-blockHome__title"><?php echo $first['title'] ?></h2>
            <p class="c-blockHome__text"><?php echo $first['text'] ?></p>
            <?php if ($first['link']['url']) : ?>
            <a class="c-blockHome__link o-button" href="<?php echo $first['link']['url'] ?>" target="<?php echo $first['link']['target'] ?>"></a>
            <?php endif ?>
        </div>
    </div>

    <div class="c-blockHome c-blockHome__second">
        <div class="c-blockHome__content">
            <h2 class="c-blockHome__title"><?php echo $second['title'] ?></h2>
            <p class="c-blockHome__text"><?php echo $second['text'] ?></p>
            <?php if ($second['link']['url']) : ?>
                <a class="c-blockHome__link o-button" href="<?php echo $second['link']['url'] ?>" target="<?php echo $second['link']['target'] ?>"></a>
            <?php endif ?>
        </div>
        <div class="c-blockHome__image">
            <div class="u-cover-image">
                <img src="<?php echo $second['image2']['url'] ?>" alt="<?php echo $second['image2']['title'] ?>">
            </div>
        </div>
    </div>

    <div class="c-blockHome c-blockHome__third">
        <div class="c-blockHome__image">
            <div class="u-cover-image">
                <img src="<?php echo $third['image']['url'] ?>" alt="<?php echo $third['image']['title'] ?>">
            </div>
        </div>
        <div class="c-blockHome__content">
            <h2 class="c-blockHome__title"><?php echo $third['title'] ?></h2>
            <p class="c-blockHome__text"><?php echo $third['text'] ?></p>
            <?php if ($third['link']['url']) : ?>
                <a class="c-blockHome__link o-button" href="<?php echo $third['link']['url'] ?>" target="<?php echo $third['link']['target'] ?>"></a>
            <?php endif ?>
        </div>
    </div>
</section>