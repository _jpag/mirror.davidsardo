
<?php if ( have_rows('opens') ) : ?>
    <a href="#calendar" class="js-scrollCalendar c-calendar__btn"></a>
    <section id="calendar" class="c-calendar">
        <h2 class="c-opens__title"><?php the_field('title') ?></h2>
        <div class="c-opens__inner">
            <?php while( have_rows('opens') ) : the_row(); ?>
                <div class="c-opens">
                <?php $month = get_sub_field('month_group'); ?>
                <h4 class="c-opens__month it"><?php echo $month['month_it']; ?></h4>
                <h4 class="c-opens__month en"><?php echo $month['month_en']; ?></h4>
                <h4 class="c-opens__month de"><?php echo $month['month_de']; ?></h4>
                <h4 class="c-opens__month sl"><?php echo $month['month_sl']; ?></h4>
                    
                    <?php if ( have_rows('days') ) : ?>
                        <?php while( have_rows('days') ) : the_row(); ?>
                        <p>
                            <span><?php the_sub_field('from'); ?>.</span>
                            <?php if (get_sub_field('to')) : ?>
                            <span>&nbsp;–&nbsp;</span>
                            <span><?php the_sub_field('to'); ?>.</span>
                            <?php endif; ?></p>
                        <?php endwhile; ?>
                    <?php endif; ?>

                </div>
            <?php endwhile; ?>
        </div>
        <p class="c-calendar__note it"><?php the_field('note_it') ?></p>
        <p class="c-calendar__note en"><?php the_field('note_en') ?></p>
        <p class="c-calendar__note de"><?php the_field('note_de') ?></p>
        <p class="c-calendar__note sl"><?php the_field('note_sl') ?></p>
    </section>
<?php endif; ?>
    