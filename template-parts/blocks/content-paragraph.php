<?php 
    $paragraph = get_field('paragraph');
    $title = $paragraph['title'];
    $text = $paragraph['text'];
?>
<?php if($text): ?>
    <section class="c-paragraph">
        <?php if($title): ?>
            <h2 class="c-paragraph__title"><?php echo $title ?></h2>
        <?php endif; ?>
        <?php echo $text ?>
    </section>
<?php endif; ?>