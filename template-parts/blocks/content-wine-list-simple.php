<?php 

?>
<section class="c-wineList__simple">
    <?php if ( have_rows('row') ) : ?>
    <?php while( have_rows('row') ) : the_row(); 
        $image = get_sub_field('image');
        $text = get_sub_field('text'); ?>

        <div class="c-wineList__col">
            <div class="c-wineList__cel image">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
            </div>
            <?php if ($text) : ?>
                <div class="c-wineList__cel texts">
                    <?php echo $text; ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endwhile; ?>
    <?php endif; ?>
</section>