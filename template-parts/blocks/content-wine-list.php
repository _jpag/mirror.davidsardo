<?php 

?>
<section class="c-wineList">
    <?php if ( have_rows('row') ) : ?>
    <?php while( have_rows('row') ) : the_row(); 
        $image = get_sub_field('image');
        $col1 = get_sub_field('col_1');
        $col2 = get_sub_field('col_2');
        $col3 = get_sub_field('col_3'); 
        $image_alt = get_sub_field('image_alt'); ?>

        <div class="c-wineList__row">
            <div class="c-wineList__cel image">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
            </div>
            <?php if ($col1['title']) : ?>
                <div class="c-wineList__cel texts">
                    <h5 class="c-wineList__title"><?php echo $col1['title']; ?></h5>
                    <?php echo $col1['text']; ?>
                </div>
            <?php endif; ?>

            <?php if ($col2['title']) : ?>
                <div class="c-wineList__cel texts">
                    <h5 class="c-wineList__title"><?php echo $col2['title']; ?></h5>
                    <?php echo $col2['text']; ?>
                </div>
            <?php endif; ?>

            <?php if ($col3['title']) : ?>
                <div class="c-wineList__cel texts">
                    <h5 class="c-wineList__title"><?php echo $col3['title']; ?></h5>
                    <?php echo $col3['text']; ?>
                </div>
            <?php endif; ?>

            <?php if ($image_alt) : ?>
                <div class="c-wineList__imageAlt" style="background-image: url('<?php echo $image_alt['url'] ?>')"></div>
            <?php endif; ?>

        </div>
    <?php endwhile; ?>
    <?php endif; ?>
</section>