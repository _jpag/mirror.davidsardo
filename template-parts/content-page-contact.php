<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BrainBlank
 */

$map = get_field('map');
$address = get_field('address');
$form = get_field('form');
?>

<article>
	<header class="c-page__header">
		<div class="l-container c-page__titleContainer">
			<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
		</div>
		<?php if($address): ?>
		<div class="c-contact__info desktop">
			<div class="l-container">				
				<?php if($address['title']): ?>
				<h2 class="c-paragraph__title"><?php echo $address['title'] ?></h2>
				<?php endif; ?>
				<?php echo $address['text'] ?>
			</div>
		</div>
		<?php endif; ?>

		<div class="c-page__image">
			<?php if ($map) : ?>
			<div id="map" class="c-contact__map"></div>
			<?php else: ?>
			<div class="u-cover-image">
				<?php the_post_thumbnail('thumb_page'); ?>
			</div>
			<?php endif; ?>
		</div>
	</header>

	<div class="c-page__body <?php echo get_field('image') ?>">
		<div class="l-container">
		<?php if($address): ?>
		<div class="c-contact__info mobile">
			<?php if($address['title']): ?>
			<h2 class="c-paragraph__title"><?php echo $address['title'] ?></h2>
			<?php endif; ?>
			<?php echo $address['text'] ?>
		</div>
		<?php endif; ?>

			<div class="c-contact__form">
				<?php if($address['title_form']): ?>
					<h2 class="c-paragraph__title"><?php echo $address['title_form'] ?></h2>
				<?php endif; ?>
				<?php echo $address['form'] ?>
			</div>
			<?php the_content(); ?>
		</div>
	</div>

	<?php if ( get_edit_post_link() ) : ?>
	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'brainblank' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
	</footer><!-- .entry-footer -->
	<?php endif; ?>
</article>

<script>
	// Initialize and add the map
	function initMap() {
	var position = {lat: <?php echo $map['lat'] ?>, lng: <?php echo $map['lng'] ?>};
	var map = new google.maps.Map(
		document.getElementById('map'), {
			zoom: 15, 
			center: position,
			disableDefaultUI: true,
			});
	var infowindow = new google.maps.InfoWindow({
		content: '<strong>OSMIZA SARDO DAVID</strong><br><br><a class="o-button small" href="https://www.google.com/maps/dir/?api=1&destination='+<?php echo $map['lat'] ?>+','+<?php echo $map['lng'] ?>+'" target="_blank">OTTIENI INDICAZIONI</a></strong>'
	});
	var icon = {
		url: '<?php echo $map['pin'] ?>',
		scaledSize: new google.maps.Size(50, 50),
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(0, 0)
	};
	var marker = new google.maps.Marker({
		position: position, 
		map: map,
		icon:icon,
		size: (80, 80),
		});
	marker.addListener('click', function() {
		infowindow.open(map, marker);
	});
	// Create a new StyledMapType object, passing it an array of styles,
	// and the name to be displayed on the map type control.
	var styledMapType = new google.maps.StyledMapType(
		[
			{
				"featureType": "administrative",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#444444"
					}
				]
			},
			{
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [
					{
						"color": "#f2f2f2"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi.business",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"visibility": "on"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 45
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "all",
				"stylers": [
					{
						"color": "#b4d4e1"
					},
					{
						"visibility": "on"
					}
				]
			}
		],
		{name: 'Styled Map'});


	//Associate the styled map with the MapTypeId and set it to display.
	map.mapTypes.set('styled_map', styledMapType);
	map.setMapTypeId('styled_map');
	}
		</script>
		<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3JC1NZgzElfx7VNXOWpZJ0BsJOTGaU0&callback=initMap">
</script>
