<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BrainBlank
 */
?>

<article>
	<header class="c-page__header">
		<div class="l-container c-page__titleContainer">
				<?php the_title( '<h1 class="c-page__title">', '</h1>' ); ?>
		</div>
		<div class="c-page__image">
			<div class="u-cover-image">
				<?php the_post_thumbnail('thumb_page'); ?>
			</div>
		</div>
	</header><!-- .entry-header -->

	<div class="c-page__body <?php echo get_field('image') ?>">
		<div class="l-container">
			<?php the_content(); ?>
		</div>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'brainblank' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article>
